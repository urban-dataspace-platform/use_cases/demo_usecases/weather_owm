
# Demo Use-Case OpenWeatherMap

## Projektbeschreibung

In diesem Use-Case wurde die Open Data API von [OpenWeatherMap](https://openweathermap.org/) eingebunden. Die API liefert weltweite Wetterdaten und -vorhersagen die aus mehreren Quellen aggregiert werden.

Der Use-Case wurde auf der FIWARE-basierten [Urban Dataspace Platform](https://gitlab.com/urban-dataspace-platform/core-platform) entwickelt und integriert.

## Installation

Für die Installation und Konfiguration dieses Use-Cases wird auf das [deployment repository](https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/deployment) verwiesen

## Funktionsweise

Der NodeRED-Flow ruft die Daten der konfigurierten Station per HTTP-Request von der API ab. Diese Daten werden in die NGSI-kompatiblen [WeatherObserved](https://github.com/smart-data-models/dataModel.Weather/blob/master/WeatherObserved/README.md) und [WeatherForecast](https://github.com/smart-data-models/dataModel.Weather/blob/master/WeatherForecast/README.md) Smart-Data-Models umgewandelt und an die Datenplattform gesendet. 


## Kontaktinformationen

Dieses Projekt wurde von der [Hypertegrity AG](https://www.hypertegrity.de/) entwickelt.

## Link zum Original-Repository

https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/weather_owm